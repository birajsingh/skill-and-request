package com.company;

import java.util.HashSet;

public class mn {
    public static void main(String[] args) {
        String[] number_one = { "java" , "js"  , "angular" , "react" , "c++" };
        String[] number_two = { "js" , "angular" , "c"  , "react"};


        HashSet<String> matched = new HashSet<String>();
        HashSet<String> mismatched = new HashSet<String>();
        for (int i = 0; i < number_one.length; i++) {
            mismatched.add(number_one[i]);
        }

        for (int i = 0; i < number_two.length; i++) {
            mismatched.add(number_two[i]);
        }

        for(int n = 0; n < number_one.length; n++) {
            for (int m = 0; m < number_two.length; m++) {
                if(number_one[n].equals(number_two[m])){
                    matched.add(number_one[n]);
                }
            }
        }
        for (int i = 0; i < matched.size(); i++) {
            mismatched.remove(matched.toArray()[i]);
        }

        System.out.print("Matched: \n");
        for (int i = 0; i < matched.size(); i++) {
            System.out.print(matched.toArray()[i] + " ");
        }

        System.out.println("");
        System.out.print("Mismatched: ");
        for (int i = 0; i < mismatched.size(); i++) {
            System.out.print(mismatched.toArray()[i] + " ");
        }
    }
}
