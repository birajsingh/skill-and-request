package com.company;

import java.text.DecimalFormat;

public class skill {
    public static void main(String[] args) {
        //cv skills
        String[] database = {"mysql", "postgres","microsoft sql server", "mongodb"};
        String[] framework = {"spring", "bootstrap", "hibernate"};
        String[] web = {"css", "jsp", "html",};
        String[] swTools = {"eclipse", "sublime", "intellij"};
        String[] dataScientistSkill = {"aws"};
        String[] os = {"windows"};
        String[] finance = {"management", "logic"};
        String[] programmingLanguage = {"java", "javascript", "python"};


        //jd skills
        String[] database_jd = {"postgres", "mysql", "microsoft sql server", "mongodb"};
        String[] framework_jd = {"spring", "bootstrap", "hibernate", "jwt", "spring boot"};
        String[] web_jd = {"css", "jsp", "html", "mvc", "jquery"};
        String[] swTools_jd = {"brackets", "eclipse", "intellij", "spring tool suite"};
        String[] dataScientistSkill_jd = {"aws","hadoop"};
        String[] os_jd = {"linux", "windows"};
        String[] finance_jd = {"management", "logic", "communication"};
        String[] programmingLanguage_jd = {"java", "javascript", "c++", "c", "python", "es"};


        //length of jd skills(count of jd skills)
        float databaseLength = database_jd.length;
        float frameworkLength = framework_jd.length;
        float webLength = web_jd.length;
        float swtoolsLength = swTools_jd.length;
        float dataScientistLength = dataScientistSkill_jd.length;
        float osLength = os_jd.length;
        float financeLength = finance_jd.length;
        float programmingLanguageLength = programmingLanguage_jd.length;

        float totalLength = databaseLength + frameworkLength + webLength + swtoolsLength + dataScientistLength + osLength +
                financeLength + programmingLanguageLength;

        int i, j;
        int databaseCount = 0, frameworkCount = 0, webCount = 0, softwareToolsCount = 0, dataScientistCount = 0,
                osCount = 0, financeCount = 0, programmingLanguageCount = 0;

        DecimalFormat f = new DecimalFormat("##.0");

        //database
        for (i = 0; i < database.length; i++) {
            for (j = 0; j < database_jd.length; j++) {
                if (database[i] == database_jd[j]) {
                    databaseCount++;
//                        databaseCount+=database[i].length();
//                    System.out.println(database[i]);
                }
            }
        }
        //framework
        for (i = 0; i < framework.length; i++) {
            for (j = 0; j < framework_jd.length; j++) {
                if (framework[i].equals(framework_jd[j])) {
                    frameworkCount++;
                }
            }
        }

        //web
        for (i = 0; i < web.length; i++) {
            for (j = 0; j < web_jd.length; j++) {
                if (web[i].equals(web_jd[j])) {
                    webCount++;
                }
            }
        }

        //sw tools
        for (i = 0; i < swTools.length; i++) {
            for (j = 0; j < swTools_jd.length; j++) {
                if (swTools[i].equals(swTools_jd[j])) {
                    softwareToolsCount++;
                }
            }
        }

        //data scientist skill
        for (i = 0; i < dataScientistSkill.length; i++) {
            for (j = 0; j < dataScientistSkill_jd.length; j++) {
                if (dataScientistSkill[i].equals(dataScientistSkill_jd[j])) {
                    dataScientistCount++;
                }
            }
        }

        //os
        for (i = 0; i < os.length; i++) {
            for (j = 0; j < os_jd.length; j++) {
                if (os[i].equals(os_jd[j])) {
                    osCount++;
                }
            }
        }

        //finance
        for (i = 0; i < finance.length; i++) {
            for (j = 0; j < finance_jd.length; j++) {
                if (finance[i].equals(finance_jd[j])) {
                    financeCount++;
                }
            }
        }

        //programming language
        for (i = 0; i < programmingLanguage.length; i++) {
            for (j = 0; j < programmingLanguage_jd.length; j++) {
                if (programmingLanguage[i].equals(programmingLanguage_jd[j])) {
                    programmingLanguageCount++;
                }
            }
        }



//        System.out.println(databaseCount);
//        System.out.println(totalLength);
        float database_Score = (55 / totalLength) * databaseCount;
        float framework_score = (55 / totalLength) * frameworkCount;
        float web_Score = (55 / totalLength) * webCount;
        float swTools_Score = (55 / totalLength) * softwareToolsCount;
        float dataScientist_Score = (55 / totalLength) * dataScientistCount;
        float os_Score = (55 / totalLength) * osCount;
        float finance_Score = (55 / totalLength) * financeCount;
        float programmingLanguage_Score = (55 / totalLength) * programmingLanguageCount;
        System.out.println(f.format(database_Score));
        System.out.println(f.format(framework_score));
        System.out.println(f.format(web_Score));
        System.out.println(f.format(swTools_Score));
        System.out.println(f.format(dataScientist_Score));
        System.out.println(f.format(os_Score));
        System.out.println(f.format(finance_Score));
        System.out.println(f.format(programmingLanguage_Score));

        float final_score = database_Score + framework_score + web_Score + swTools_Score + dataScientist_Score +
                os_Score + finance_Score + programmingLanguage_Score;
        System.out.println("Score: "+f.format(final_score));

    }
}
